#include "ft_recursive_factorial.h"

int ft_recursive_factorial(int nb)
{
	int i;
	int result;

	i = nb;
	result = 1;

	if (i == nb)
	{
		result = ft_recursive_factorial(i) * (i-1);
	}
	i--;
	return result;
}