#include "ft_fibonacci.h"

void specialSuite(int index, int *result)
{
	if (index < 0)
	{
		*result = -1;
		return;
	}
	
	else if (index == 0)
	{
		*result = 0;
		return;
	}
	
	else if (index == 1)
	{
		*result = 1;
		return;
	}	
}

void fibonacciCalc(int index, int *result)
{
	int index0 = 0;
	int index1 = 1;

	while (index > 1)
	{
		*result = index0 + index1;
		index0 = index1;
		index1 = *result;
		index--;
	}
}

int ft_fibonacci(int index)
{
	int result;
	result = 0;

	if (index < 2)
	{
		specialSuite(index, &result);
	}

	else
	{
		fibonacciCalc(index, &result);
	}

	return result;
}
