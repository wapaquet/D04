#include "ft_recursive_power.h"

void powerSpecial(int nb, int *pointResult, int power)
{
	if (power == 1)
	{
		return;
	}

	else if (power == 0)
	{
		*pointResult = 1;
	}

	else if (power < 0)	
	{
		*pointResult = 0;
	}
}

void recursivePowerSup1(int nb, int *pointResult, int power)
{
	if (power > 1)
	{
		*pointResult *= nb;
		recursivePowerSup1(nb, pointResult, power-1);
	}
}

int ft_recursive_power(int nb, int power)
{	
	int result;
	result = nb;

	if (power <= 1)
	{
		powerSpecial(nb, &result, power);
	}
	
	if (power > 1)
	{
		recursivePowerSup1(nb, &result, power);
	}

	return result;
}
