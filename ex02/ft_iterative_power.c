#include "ft_iterative_power.h"

int ft_iterative_power (int nb, int power) 
{
	int result;
	result = nb;

	if (power == 1)
	{
		return nb;
	}

	else if (power == 0)
	{
		result = 1;
	}

	else if (power < 0)	
	{
		result = 0;
	}

	while (power > 1)
	{
		result *= nb;
		power--;
	}

	return result;
}
