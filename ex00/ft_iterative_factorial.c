#include "ft_iterative_factorial.h"

int ft_iterative_factorial(int nb)
{
	int i;
	int result;
	
	i = 1;
	result = 1;
	
	while (i <= nb)
	{
		result = result*i;
		i++;
	}

	return result;
}
